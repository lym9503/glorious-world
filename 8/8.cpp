#include<iostream>
#include<queue>
#include<algorithm>
#include<stdio.h>
#include<string>
#include<cstring>
using namespace std;

#define maxn 102

char a[maxn][maxn][maxn]; // saving input characters
bool visited[maxn][maxn][maxn];
int withbev[maxn][maxn][maxn]; // whether there is beverage in the room. 
				// 0 for "without", 1 for "with" 
int bev[maxn][maxn][maxn]; // the maximum number of bottles of beverage that Small2Kuo can drink
			// when he reaches each room by the shortest path
int len[maxn][maxn][maxn]; // the minimum length of path from starting room to each room
int n;

inline void readchar(char& c){ // it is to avoid to read characters like '\r' and '\n'
	while(1){
		scanf("%c",&c);
		if(c!='\r'&&c!='\n')
			break;
	}
}

struct ternary{ // representing coordinates
	int a,b,c;
	ternary(){}
	ternary(int aa,int bb,int cc){
		a=aa;
		b=bb;
		c=cc;
	}
	void assign(int aa,int bb,int cc){
		a=aa;
		b=bb;
		c=cc;
	}
	bool operator ==(const ternary& t) const{
		return a==t.a && b==t.b && c==t.c;
	}
};

bool rational(int i){ // if the index $i is within bound
	return i>=0&&i<n;
}

const int da[]={-1,0,0,0,0,1}; // delta coordinate
const int db[]={0,-1,0,1,0,0};
const int dc[]={0,0,1,0,-1,0};

queue<ternary> qt;

ternary mystart,myend; // coordinates of starting room and ending room

int main()
{
	//freopen("hw4p5.in","r",stdin);
	//freopen("hw4p5.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--){
		memset(visited,false,sizeof(visited));
		memset(bev,0,sizeof(bev));
		memset(len,0,sizeof(len));
		memset(withbev,0,sizeof(withbev));
		while(!qt.empty()) // clear the queue
			qt.pop();
		scanf("%d",&n);
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				getchar();
				for(int k=0;k<n;k++)
				{
					readchar(a[i][j][k]);
					if(a[i][j][k]=='S') // starting room
						mystart.assign(i,j,k);
					else if(a[i][j][k]=='E') // ending room
						myend.assign(i,j,k);
					else if(a[i][j][k]=='B') // room with beverage
						withbev[i][j][k]=1;
				}
			}
		}
		qt.push(mystart);
		visited[mystart.a][mystart.b][mystart.c]=true;
		while(!qt.empty()){
			ternary thist=qt.front();
			if(thist==myend)
				break;
			qt.pop();
			for(int i=0;i<6;i++){
				int na=thist.a+da[i]; // calculate the coordinates of the possible next room
				int nb=thist.b+db[i];
				int nc=thist.c+dc[i];
				// if the room exists and can be stepped in
				if(rational(na) && rational(nb) && rational(nc) && a[na][nb][nc]!='#'){
					// if the room has already been visited, then update the $bev value if
					// the current path is also the shortest
					if(visited[na][nb][nc]){
						if(len[na][nb][nc]==len[thist.a][thist.b][thist.c]+1){
							bev[na][nb][nc]=max(bev[na][nb][nc],bev[thist.a][thist.b][thist.c]+withbev[na][nb][nc]);
						}
						continue;
					}
					visited[na][nb][nc]=true;
					len[na][nb][nc]=len[thist.a][thist.b][thist.c]+1;
					bev[na][nb][nc]=bev[thist.a][thist.b][thist.c]+withbev[na][nb][nc];
					qt.push(ternary(na,nb,nc));
				}
			}
		}
		if(!visited[myend.a][myend.b][myend.c]) // unable to reach the ending room
			printf("Fail OAQ\n");
		else // Victory! Got out!
			printf("%d %d\n",len[myend.a][myend.b][myend.c],bev[myend.a][myend.b][myend.c]);
	}
	return 0;
}
