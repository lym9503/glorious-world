// Much thanks to classmate Chengyang Lv and TA Wally Wei for help

/*

Key to the algorithm: Convert the problem to its "supplementary problem", which is more familiar to 
us and can be solved by greedy algorithm.
The "supplementary problem" of this problem is: First take all coins out of the wallet. Then put
back coins as fewer as possible with a fixed sum of value.
In the comment below, I will use the word "throw" for "put back", because the coins that are put
back to the wallet make no contribution to the price of beverage.

*/

#include<iostream>
#include<set>
#include<map>
#include<stdio.h>
#include<stdlib.h>
#include<cmath>
#include<vector>
#include<algorithm>
#include<string>
#include<cstring>
#include<stack>
#include<queue>
#include<list>
#include<valarray>
#include<numeric>
#include<sstream>
using namespace std;

#define typenum 10 // number of kinds of coins

int v[10]={1,5,10,20,50,100,200,500,1000,2000};
int coincnt[12]; // number of different kinds of coins Small2Kuo has
int coinaway[21][12]; // number of coins thrown away, compared with the state that all coins are taken
					// coinaway[0][.] : the array for iterative solving
					// coinaway[1-8][.] : arrays for backup

void backup(int option) // copy coinaway[0] to coinaway[option]
{
	memcpy(coinaway[option],coinaway[0],sizeof(coinaway[0]));
}

void restore(int option) // restore coinaway[0] from coinaway[option]
{
	memcpy(coinaway[0],coinaway[option],sizeof(coinaway[option]));
}

int totalaway() // total number of coins thrown away
{
	int ans=0;
	for(int i=0;i<typenum;i++)
		ans+=coinaway[0][i];
	return ans;
}

int solve(int depth,int money) // depth: which coin is now being considered throwing away
								// from 9 (for 2000 NTD) down to 0 (for 1 NTD)
								// money: how much money is yet to be thrown away
								// If a solution is found, return 0; if no solution, return -1.
{
	if(money==0) // Bingo! A solution is found.
		return 0;
	if(depth==-1) // All kinds of coins have been considered, but some value of money failed
		return -1;				//  to be thrown away. No solution.
	int num=min(coincnt[depth],money/v[depth]); // maximum number of this kind of coin that can be
												//  thrown away so that $money is not negative

	if(depth!=0 && num){ // if at least one coin can be throw away, then we consider the two solutions
						// when we throw away $num coins and ($num-1) coins

		// Case 1: Throw away $num coins
		backup(depth*2-1); // backup the current state (when we have considered coins 9,8,...,depth+1)
							// to coinaway[depth*2+1]
		coinaway[0][depth]=num;
		int solve1=solve(depth-1,money-v[depth]*num);
		int ans1=totalaway(); // total number of coins thrown in case 1
		backup(depth*2); // save this solution (number of each kind of coin thrown) in coinaway[depth*2]

		// Case 2: Throw away ($num -1) coins
		restore(depth*2-1);
		coinaway[0][depth]=num-1;
		int solve2=solve(depth-1,money-v[depth]*(num-1));
		int ans2=totalaway();

		if(solve1==-1 && solve2==-1) // no solution
			return -1;
		if(solve2==-1 || (solve1!=-1 && solve2!=-1 && ans1<ans2)){ 
						// Solution of case 1 is a better (or the only) solution
			restore(depth*2); // Solution 1, I need you!
			return 0;
		}
		else // Solution 2 is a better (or the only) solution, which is just in coinaway[0] now
			return 0;	// and does not need to be restored
	}
	else{ // otherwise throw away coins as many as possible
		coinaway[0][depth]=num;
		return solve(depth-1,money-v[depth]*num);
	}
}

int main()
{
	int t;
	scanf("%d",&t);
	while(t--)
	{
		memset(coinaway,0,sizeof(coinaway));
		int p;
		scanf("%d",&p);
		for(int i=0;i<typenum;i++)
			scanf("%d",&coincnt[i]);

		int sum=0;
		for(int i=0;i<typenum;i++)
			sum+=v[i]*coincnt[i];
		sum-=p; // sum: total value needed to be deducted, from the state that all coins are taken

		if(sum<0){ // Case 1: Too poor to buy beverage. Poor Small2Kuo!
			printf("-1\n");
			continue;
		}
		if(solve(typenum-1,sum)==-1){ // Case 2: Although money is enough, it is impossible to gather together
										// coins with value of exactly the price of the beverage. 
										// What an unlucky guy!
			printf("-1\n");
			continue;
		}
		else{ // Case 3: Enjoy the beverage!
			int ans=0;
			for(int i=0;i<typenum;i++)
				ans=ans+coincnt[i]-coinaway[0][i];
			printf("%d\n",ans);
		}
	}

	return 0;
}