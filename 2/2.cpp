//Much thanks to TA Wei Youlin and Xu Yuxuan for help.
//Yiming Luo T03902103

#include<iostream>
#include<algorithm>
using namespace std;

struct box
{
	long long x,y;
	bool operator <(const box& b) const
	{
		if(x<b.x)
			return true;
		else if(x==b.x && y<b.y)
			return true;
		return false;
	}
	bool operator ==(const box& b)
	{
		return x==b.x && y==b.y;
	}
};

box b[200000];
box buffer[200000];

void mergesort(int left,int right)
{
	if(left==right)
		return;
	int mid=left+(right-left)/2;
	mergesort(left,mid);
	mergesort(mid+1,right);
	int ptr1=left,ptr2=mid+1,ptr3=left;
	while(ptr1!=mid+1)
	{
		while(ptr2!=right+1 && b[ptr2]<b[ptr1])
		{
			buffer[ptr3++]=b[ptr2++];
		}
		buffer[ptr3++]=b[ptr1++];
	}
	while(ptr2!=right+1)
	{
		buffer[ptr3++]=b[ptr2++];
	}
	for(int i=left;i<=right;i++)
	{
		b[i]=buffer[i];
	}
}

long long mymerge(long long left,long long mid,long long right)
{
	long long ptr1=left,ptr2=mid+1,ptr3=left;
	long long ans=0;
	long long repeatcnt=0;
	while(ptr2!=right+1)
	{
		while(ptr1!=mid+1 && b[ptr2].y>=b[ptr1].y)
		{
			//if(b[ptr1].x==b[ptr2].x && b[ptr1].y==b[ptr2].y)
			//	ans++;
			//if(ptr1!=left&&b[ptr1]==b[ptr1-1])
			//	repeatcnt++;
			buffer[ptr3++]=b[ptr1++];
		}
		ans+=ptr1-left+repeatcnt;
		buffer[ptr3++]=b[ptr2++];
	}
	if(b[mid+1]==b[mid])
	{
		while(ptr1>=left&&b[ptr1]==b[mid])
			ptr1--;
		while(ptr2<=right&&b[ptr2]==b[mid+1])
			ptr2++;
		ans+=(mid-ptr1)*(ptr2-mid-1);
	}
	while(ptr1!=mid+1)
		buffer[ptr3++]=b[ptr1++];
	for(long long i=left;i<=right;i++)
		b[i]=buffer[i];
	return ans;
}

long long conquer(long long left,long long right)
{
	if(left==right)
		return 0;
	//if(b[left]==b[right])
	//{
	//	long long tmp=right-left+1;
	//	return tmp*(tmp-1);
	//}
	long long mid=left+(right-left)/2;
	long long ans=0;
	ans+=conquer(left,mid);
	ans+=conquer(mid+1,right);
	ans+=mymerge(left,mid,right);
	return ans;
	//return conquer(left,mid)+conquer(mid+1,right)+mymerge(left,mid,right);
}

int main()
{
	//freopen("boxes.in","r",stdin);
	//freopen("boxes.out","w",stdout);
	long long t,n;
	scanf("%lld",&t);
	while(t--)
	{
		scanf("%lld",&n);
		for(long long i=0;i<n;i++)
		{
			scanf("%lld%lld",&b[i].x,&b[i].y);
			if(b[i].x>b[i].y)
			{
				long long temp=b[i].x;
				b[i].x=b[i].y;
				b[i].y=temp;
			}
		}
		mergesort(0,n-1);
		//printf("%lld\n",conquer(0,n-1));
		cout<<conquer(0,n-1)<<endl;
	}
	return 0;
}

