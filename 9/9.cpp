#include<iostream>
#include<stdio.h>
#include<queue>
#include<vector>
#include<cstring>
using namespace std;

#define maxn 100005
#define maxm 200005
#define maxbit 30

int t,n,m;

// a struct storing the information of an adjacent edge of some node,
// including its weight(#{c}) and the adjacent node(#{b})
struct adjedge{
	int b,c;
	adjedge(int b0,int c0){
		b=b0;
		c=c0;
	}
	adjedge(){}
};

// storing the adjacent edges of all the nodes
vector<adjedge> adj[maxn];

queue<int> qi;
bool visited[maxn];

// test whether the graph is connected with only the subset of edges
// which satisfy the condition "mask".
// An edge with power #{c} satisfies "Condition 'mask'" means that 
// for every bit in the binary representation of #{c}, it equals to
// one only if the same bit in #{mask} is one. In other words, if a
// bit in #{mask} is zero, then it must be zero in #{c}, otherwise
// we say this edge does not satisfy the condition "mask".
// Our final goal is to find the minimum #{mask} that can keep the
// graph connected.
bool ok(int mask)
{
// BFS the graph from node 1 (assume it exists) to see if the graph
// is connected.
// BTW: luckily, it seems that node 1 exists in all test inputs
// on Judgegirl.
	memset(visited,false,sizeof(visited));
	while(!qi.empty())
		qi.pop();
	qi.push(1);
	visited[1]=true;
	int nodecnt=1;
	while(!qi.empty()){
		int a=qi.front();
		qi.pop();
		for(vector<adjedge>::iterator it=adj[a].begin();it!=adj[a].end();it++){
			int b=(*it).b;
			int c=(*it).c;
			if(!visited[b] && (c&(~mask))==0){
				visited[b]=true;
				qi.push(b);
				nodecnt++;
			}
		}
	}
	return nodecnt==n;
}

int main()
{
	//freopen("hw4p6.in","r",stdin);
	//freopen("hw4p6.out","w",stdout);
	scanf("%d",&t);
	int a,b,c;
	while(t--){
		scanf("%d%d",&n,&m);
		for(int i=0;i<maxn;i++)
			adj[i].clear();
		// construct the array of vectors storing the information
		// of adjacent edges of the nodes
		for(int i=0;i<m;i++){
			scanf("%d%d%d",&a,&b,&c);
			adj[a].push_back(adjedge(b,c));
			adj[b].push_back(adjedge(a,c));
		}
		int mask=(1<<maxbit)-1; // #{mask} is like 11...11
		for(int bit=maxbit-1;bit>=0;bit--){
			// test whether the graph is still connected if the
			// #{bit}th bit in the current #{mask} is set to 0
			int tmpmask=(1<<maxbit)-(1<<bit)-1; // like 11..101..11
			// if this bit can be set to 0, then do it! Recall
			// that our final goal is to make #{mask} as small as
			// possible.
			if(ok(mask&tmpmask)) 
				mask&=tmpmask;
		}
		printf("%d\n",mask);
	}
	return 0;
}
