#include<iostream>
#include<string>
#include<cstring>
#include<queue>
#include<list>
#include<algorithm>
using namespace std;

#define BIT 32

// interger with 124 bits
struct myint{
	int x[4];
	inline void operator =(myint y);
	inline void operator =(int y);
	inline myint operator |(myint& y);
	inline void operator |=(myint& y);
	inline bool operator ==(myint& y);
	inline bool operator ==(int y);
	myint(){}
	myint(int x);
	inline bool isOne(int bit){
		return x[bit/BIT]&(1<<(bit%BIT));
	}
};

inline void myint::operator =(myint y){
		memcpy(x,y.x,4*sizeof(int));
}

inline void myint::operator =(int y){
	x[0]=y;
	x[1]=x[2]=x[3]=0;
}

inline bool myint::operator ==(myint& y){
	return memcmp(x,y.x,4*sizeof(int));
}

inline bool myint::operator ==(int y){
	return x[0]==y;
}

inline myint myint::operator |(myint& y){
	myint ans;
	for(int i=0;i<4;i++)
		ans.x[i]=x[i]|y.x[i];
	return ans;
}

inline void myint::operator |=(myint& y){
	for(int i=0;i<4;i++)
		x[i]|=y.x[i];
}

myint::myint(int x0){
	x[0]=x0;
	x[1]=x[2]=x[3]=0;
}


inline void setone(myint& mx,int k){
	mx.x[k/BIT]|=1<<(k%BIT);
}

inline void setzero(myint& mx,int k){
	mx.x[k/BIT]&=~(1<<(k%BIT));
}

inline bool compatible(myint nodeforbid,int newnode){
	return !(nodeforbid.x[newnode/BIT]&(1<<(newnode%BIT)));
}

#define maxn 102
myint adj[maxn];
myint nodeforbid[maxn];
int thisnode[maxn];
int pos;
int branch[maxn];
int nextnodeinbranch[maxn];
int firstnodeinbranch[maxn];
list<int> adjl[maxn];
bool exist[maxn];
int nodesleftinbranch[maxn];

inline void disappear(int n){
	while(!adjl[n].empty()){
		int tmp=adjl[n].back();
		adjl[tmp].remove(n);
		adjl[n].remove(tmp);
		setzero(adj[tmp],n);
		setzero(adj[n],tmp);
	}
	exist[n]=false;
}

int t,n,m;

void printlist(){
	for(int i=0;i<n;i++){
		printf("%d: %d, ",i,exist[i]);
		for(list<int>::iterator it=adjl[i].begin();it!=adjl[i].end();it++)
			printf("%d ",*it);
		printf("\n");
	}
	
	printf("-------------------------------\n");
}

int main()
{
	//freopen("hw5p6.in","r",stdin);
	//freopen("hw5p6.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		// initialization
		memset(adj,0,sizeof(adj));
		memset(exist,true,sizeof(exist));
		scanf("%d%d",&n,&m);
		for(int i=0;i<n;i++){
			adjl[i].clear();
		}
		if(m==0){
			printf("%d\n",n);
			continue;
		}
		for(int i=0;i<n;i++)
			setone(adj[i],i);

		// input
		int a,b;
		for(int i=0;i<m;i++){
			scanf("%d%d",&a,&b);
			setone(adj[a],b);
			setone(adj[b],a);
			adjl[a].push_back(b);
			adjl[b].push_back(a);
		}

		int ans=0; // the final answer

		
		// if a node has only one neighbor, then put it into the set,
		// and at the same time, disable the neighbor together with itself
		while(1){
			int dangler=-1;
			for(int i=0;i<n;i++){
				if(exist[i] && adjl[i].size()==1){
					dangler=i;
					break;
				}
			}
			if(dangler==-1)
				break;
			//printf("Dangler: %d\n",dangler);
			//printlist();
			
			int neighbor=adjl[dangler].front();
			disappear(neighbor);
			disappear(dangler);
			ans++;
			//printf("ans=%d\n",ans);
		}

		// if a node has no neighbor, then put it into the set, then disable itself
		for(int i=0;i<n;i++){
			if(exist[i] && adjl[i].empty()){
				exist[i]=false;
				ans++;
			}
		}
		

		// find connected branch
		int branchcnt=0;
		memset(branch,-1,sizeof(branch));
		for(int i=0;i<n;i++){
			if(!exist[i] || branch[i]!=-1)
				continue;
			branch[i]=branchcnt;
			queue<int> qi;
			qi.push(i);
			while(!qi.empty()){
				int thisi=qi.front();
				qi.pop();
				for(int i=0;i<n;i++){
					if(branch[i]==-1 && adj[thisi].isOne(i)){
						branch[i]=branchcnt;
						qi.push(i);
					}
				}
			}
			branchcnt++;
		}

		memset(nodesleftinbranch,0,sizeof(nodesleftinbranch));

		for(int i=0;i<branchcnt;i++){
			int nextnode=n;
			int tmpnodesleftinbranch=0;
			for(int j=n-1;j>=0;j--){
				if(branch[j]==i){
					nextnodeinbranch[j]=nextnode;
					nextnode=j;
					nodesleftinbranch[j]=tmpnodesleftinbranch++;
				}
			}
			firstnodeinbranch[i]=nextnode;
		}

		//printlist();
		// consider the maximum number of nodes that can be selected in each
		// connected branch respectively
		for(int i=0;i<branchcnt;i++){
			int maxcnt=0;
			memset(nodeforbid,-1,sizeof(nodeforbid));
			memset(thisnode,-1,sizeof(thisnode));
			pos=1; // the number of nodes selected in the branch at the time
			nodeforbid[0]=0; // nodeforbid: the nodes that cannot be selected because they
							// are adjacent to some node(s) already selected
			thisnode[1]=firstnodeinbranch[i];
			while(pos>0){
				// if it is impossible to reach a better result in this branch, then
				// give up this branch (in other words, pruning)
				if(thisnode[pos]<n){
					if(compatible(nodeforbid[pos-1],thisnode[pos])){
						maxcnt=max<int>(maxcnt,pos);
						if(pos+nodesleftinbranch[thisnode[pos]]<=maxcnt){
							pos--;
							thisnode[pos]=nextnodeinbranch[thisnode[pos]];
							continue;
						}
						nodeforbid[pos]=nodeforbid[pos-1]|adj[thisnode[pos]];
						thisnode[pos+1]=nextnodeinbranch[thisnode[pos]];
						pos++;
					}
					else{
						thisnode[pos]=nextnodeinbranch[thisnode[pos]];
					}

				}
				else{
					pos--;
					thisnode[pos]=nextnodeinbranch[thisnode[pos]];
				}
			}
			ans+=maxcnt;
		}
		printf("%d\n",ans);
	}
	return 0;
}