// Much thanks to TA Wei Youlin for help.

#include<iostream>
#include<set>
#include<map>
#include<stdio.h>
#include<stdlib.h>
#include<cmath>
#include<vector>
#include<algorithm>
#include<string>
#include<cstring>
#include<stack>
#include<queue>
#include<list>
#include<valarray>
#include<numeric>
#include<sstream>
using namespace std;

#define pb push_back
#define mp make_pair
#define all(x) (x).begin(),(x).end()
#define clr(a,val) memset(a,val,sizeof(a))
#define forn(i,n) for(int i=0;i<n;i++)
#define forv(i,v) forn(i,(v).size())
#define sz(v) v.size()
#define enter printf("\n")
#define space printf(" ")

typedef long long int LL;
typedef vector<int> VI;
typedef pair<int,int> pii;

#define maxn 16
#define MOD (1000000000+7)

int n,m;

char c[maxn][maxn+1];
int mask[maxn];
int mask2[maxn][maxn+1];
int ans[maxn][maxn+1][(1<<maxn)+1];
int firstzero[1<<maxn][maxn+1];
int zerocnt[maxn][maxn+1];
int zerowhere[maxn][maxn+1][maxn+1];

inline int encodechar(char* cc) //1 for 'X', 0 for '.'
{
	int ans=0;
	while(cc[0]!=0)
	{
		ans*=2;
		if(cc[0]=='X')
			ans++;
		cc++;
	}
	return ans;
}

inline int getkth(int nn,int k)
{
	return nn&(1<<(m-k-1));
}

inline int setzero(int nn,int k)
{
	return nn&(~(1<<(m-k-1)));
}

inline int setone(int nn,int k)
{
	return nn|(1<<(m-k-1));
}

inline void setzero2(int& nn,int k)
{
	nn&=(~(1<<(m-k-1)));
}

inline void setone2(int& nn,int k)
{
	nn|=(1<<(m-k-1));
}

inline void init(int& bx,int& by)
{
	for(bx=0;bx<n;bx++)
	{
		for(by=0;by<m;by++)
		{
			if(c[bx][by]=='.')
			{
				int k1=mask[bx]&((1<<(m-by-1))-1);
				int k2=mask[bx+1]&(~((1<<(m-by-1))-1));
				int k=k1|k2;
				ans[bx][by][k]=1;
				return;
			}
		}
	}
}

inline void initmask2()
{
	mask2[0][0]=mask[0];
	for(int i=0;i<=n-2;i++)
	{
		if(c[i+1][0]=='.')
			mask2[i][0]=setzero(mask[i],0);
		else
			mask2[i][0]=setone(mask[i],0);
		for(int j=1;j<m;j++)
		{
			if(c[i+1][j]=='.')
				mask2[i][j]=setzero(mask2[i][j-1],j);
			else
				mask2[i][j]=setone(mask2[i][j-1],j);
		}
		mask2[i][m]=mask[i+1];
	}
	mask2[n-1][0]=setzero(mask[n-1],0);
	for(int j=1;j<m;j++)
		mask2[n-1][j]=setzero(mask2[n-1][j-1],j);
}

inline void initfirstzero()
{
	for(int i=0;i<=(1<<m);i++)
	{
		int index=m;
		for(int j=m;j>=0;j--)
		{
			if(getkth(i,j)==0)
				index=j;
			firstzero[i][j]=index;
		}
	}
}

inline void myset(int& ptr,int& i,int& j,int& k,int& thisk,int wid)
{
	while(wid)
	{
		if(c[i+1][ptr]=='X')
			setone2(thisk,ptr);
		wid--;
		ptr++;
	}	
	int begin=ptr;
	int end=firstzero[k][ptr];
	int end2=end==m?end-1:end;
	if(ptr<m)
	{	
		if(i!=n-1)
		{
			//"thismask0": like "110001111"
			int thismask0=(1<<maxn)-(1<<(m-begin))+(1<<(m-end2-1))-1;
			//"thismask1": like "001110000"
			//int thismask1=1<<(m-begin-1)-1<<(m-end-1);
			int thismask=mask[i+1]&(~thismask0);
			//Set some digits of "thisk" to zero
			thisk&=thismask0;
			//Replace those digits in "thisk" with those in "thismask"
			thisk|=mask[i+1]&(~thismask0);
		}
		else
		{
			thisk&=(1<<(m-ptr-1))-1;
		}
	}
	ans[i][end][thisk]=(ans[i][end][thisk]+ans[i][j][k])%MOD;
}

void getzeros()
{
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<m;j++)
		{
			int tmp=mask2[i][j];
			int wherecnt=0;
			for(int k=0;k<m;k++)
			{
				if(!(tmp&(1<<k)))
				{
					zerowhere[i][j][wherecnt++]=k;
				}
			}
			zerocnt[i][j]=wherecnt;
		}
		if(i<n-1)
		{
			int tmp=mask[i+1];
			int wherecnt=0;
			for(int k=0;k<m;k++)
			{
				if(!(tmp&(1<<k)))
				{
					zerowhere[i][m][wherecnt++]=k;
				}
			}
			zerocnt[i][m]=wherecnt;
		}
	}
}

int main()
{
	//freopen("tetriz.in","r",stdin);
	//freopen("tetriz.out","w",stdout);
	int tc;
	scanf("%d",&tc);
	while(tc--)
	{
		memset(ans,0,sizeof(ans));
		memset(mask,0,sizeof(mask));
		memset(mask2,0,sizeof(mask2));
		memset(firstzero,0,sizeof(firstzero));
		memset(c,0,sizeof(c));
		memset(zerocnt,0,sizeof(zerocnt));
		memset(zerowhere,-1,sizeof(zerowhere));
		scanf("%d%d",&n,&m);
		initfirstzero();
		int i,j,k,l,t;
		int fir,ptr,thisk;
		for(i=0;i<n;i++)
			scanf("%s",c[i]);
		for(i=0;i<n;i++)
			mask[i]=encodechar(c[i]);
		initmask2();
		int bx,by;
		init(bx,by);
		getzeros();
		for(i=bx;i<n;i++)
		{
			for(j=by;j<m;j++)
			{
				by=0;
				if(c[i][j]=='X')
					continue;
				for(l=0;l<(1<<zerocnt[i][j]);l++)
				{
					k=mask2[i][j];
					for(int u=0;u<zerocnt[i][j];u++)
					{
						if(l&(1<<u))
						{
							k|=(1<<zerowhere[i][j][u]);
						}
					}
					if(ans[i][j][k])
					{
						fir=firstzero[k][j];
						// put 1*1 block
						thisk=k;
						ptr=j;
						myset(ptr,i,j,k,thisk,1);
						//put 1*2 block and 2*2 block
						if(j<=m-2 && getkth(k,j+1)==0)
						{
							ptr=j;
							thisk=k;
							myset(ptr,i,j,k,thisk,2);
							ptr=j;
							if(i<n-1 && j<m-1 && c[i+1][j]=='.'&&c[i+1][j+1]=='.')
							{
								thisk=k;
								setone2(thisk,j);
								setone2(thisk,j+1);
								myset(ptr,i,j,k,thisk,2);
							}
						}
						//put 2*1 block						
						if(i<n-1 && c[i+1][j]=='.')
						{
							ptr=j;
							thisk=k;
							setone2(thisk,j);
							myset(ptr,i,j,k,thisk,1);
						}
					}
				}
			}// a row ends
			if(i<n-1)
			{
				for(l=0;l<(1<<zerocnt[i][m]);l++)
				{
					int t=mask[i+1];
					for(int u=0;u<zerocnt[i][m];u++)
					{
						if(l&(1<<u))
						{
							t|=(1<<zerowhere[i][m][u]);
						}
					}
					if(!ans[i][m][t])
						continue;
					int tmp=firstzero[t|mask[i+1]][0];
					int tmp2=t;
					tmp2&=(1<<(m-tmp))-1;
					tmp2|=mask2[i+1][tmp];
					ans[i+1][tmp][tmp2]+=ans[i][m][t];
				}
			}
		}
		int finalans=0;
		for(i=0;i<(1<<m);i++)
		{
			finalans=(finalans+ans[n-1][m][i])%MOD;
		}
		printf("%d\n",finalans);
	}
	return 0;
}