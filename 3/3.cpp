//Much thanks to TA Wei Youlin and Xu Yuxuan for help.
//Yiming Luo T03902103

#include<iostream>
#include<set>
#include<map>
#include<stdio.h>
#include<stdlib.h>
#include<cmath>
#include<vector>
#include<algorithm>
#include<string>
#include<cstring>
#include<stack>
#include<queue>
#include<list>
#include<valarray>
#include<numeric>
#include<sstream>
using namespace std;

#define pb push_back
#define mp make_pair
#define all(x) (x).begin(),(x).end()
#define clr(a,val) memset(a,val,sizeof(a))
#define forn(i,n) for(int i=0;i<n;i++)
#define forv(i,v) forn(i,(v).size())
#define sz(v) v.size()
#define enter printf("\n")
#define space printf(" ")

typedef long long int LL;
typedef vector<int> VI;
typedef pair<int,int> pii;


int t,n,m,q;
int goal[100005];
int owner[100005];
int reached[100005];
int gain[100005];

int main()
{
	//freopen("minecrash.in","r",stdin);
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d%d%d",&n,&m,&q);
		for(int i=1;i<=n;i++)
			scanf("%d",&goal[i]);
		for(int i=1;i<=m;i++)
			scanf("%d",&owner[i]);
		memset(reached,-1,sizeof(reached));
		memset(gain,0,sizeof(gain));
		for(int i=1;i<=q;i++)
		{
			int left,right,unitgain;
			scanf("%d%d%d",&left,&right,&unitgain);
			for(int j=left;j<=right;j++)
			{
				int thisowner=owner[j];
				gain[thisowner]+=unitgain;
				if(reached[thisowner]==-1&&gain[thisowner]>=goal[thisowner])
					reached[thisowner]=i;
			}
		}
		bool first=true;
		for(int i=1;i<=n;i++)
		{
			if(first)
				first=false;
			else
				putchar(' ');
			printf("%d",reached[i]);
		}
		putchar('\n');
	}
	return 0;
}




