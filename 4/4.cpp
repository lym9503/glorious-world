// Much thanks to TA Wei Youlin for help. 

#include<iostream>
#include<set>
#include<map>
#include<stdio.h>
#include<stdlib.h>
#include<cmath>
#include<vector>
#include<algorithm>
#include<string>
#include<cstring>
#include<stack>
#include<queue>
#include<list>
#include<valarray>
#include<numeric>
#include<sstream>
using namespace std;

#define pb push_back
#define mp make_pair
#define all(x) (x).begin(),(x).end()
#define clr(a,val) memset(a,val,sizeof(a))
#define forn(i,n) for(int i=0;i<n;i++)
#define forv(i,v) forn(i,(v).size())
#define sz(v) v.size()
#define enter printf("\n")
#define space printf(" ")

typedef long long int LL;
typedef vector<int> VI;
typedef pair<int,int> pii;

#define maxn 2005

int next1[maxn][26];
int next2[maxn][26];
char s1[maxn],s2[maxn];
char ans[maxn];

int c[maxn][maxn];
int len1,len2;

void solve()
{
	for(int i=0;i<=len1;i++)
		c[i][len2]=0;
	for(int i=0;i<=len2;i++)
		c[len1][i]=0;
	for(int i=len1-1;i>=0;i--)
	{
		for(int j=len2-1;j>=0;j--)
		{
			int thisans1=0,thisans2=0;
			if(next2[j][s1[i]-'a']!=-1)
				thisans1=c[i+1][next2[j][s1[i]-'a']+1]+1;
			thisans2=c[i+1][j];
			int thisans=max(thisans1,thisans2);
			c[i][j]=thisans;
		}
	}
}

int main()
{
	//freopen("password.in","r",stdin);
	int t;
	scanf("%d",&t);
	while(t--)
	{
		scanf("%s%s",s1,s2);
		len1=strlen(s1);
		len2=strlen(s2);
		for(int i=0;i<26;i++)
		{
			int thisnext=-1;
			for(int j=len2-1;j>=0;j--)
			{
				if(s2[j]=='a'+i)
					thisnext=j;
				next2[j][i]=thisnext;
			}
		}
		for(int i=0;i<26;i++)
		{
			int thisnext=-1;
			for(int j=len1-1;j>=0;j--)
			{
				if(s1[j]=='a'+i)
					thisnext=j;
				next1[j][i]=thisnext;
			}
		}
		solve();
		int ptr1=0,ptr2=0;
		int index=0;
		while(index!=c[0][0])
		{
			for(int i=0;i<=25;i++)
			{
				int n1=next1[ptr1][i];
				int n2=next2[ptr2][i];
				if(n1!=-1&&n2!=-1 && c[n1+1][n2+1]==c[ptr1][ptr2]-1)
				{
					ans[index++]='a'+i;
					ptr1=n1+1;
					ptr2=n2+1;
					break;
				}
			}
		}
		ans[index]=0;
		printf("%s\n",ans);
	}
	return 0;
}
