#include<iostream>
#include<algorithm>
using namespace std;

#define maxn (100000+5)
#define INF (100000+5)

int pos[maxn];
int n,k;

int airportneeded(int t)
{
	int ans=0;
	int lastpos=pos[0];
	int lastneed=pos[0];
	for(int i=1;i<n;i++)
	{
		if(lastneed==-1 && pos[i]-lastpos>t)
			lastneed=pos[i];
		if(lastneed!=-1 && pos[i]-lastneed>t)
		{
			//  put an airport at pos i-1
			ans++;
			lastpos=pos[i-1];
			lastneed=-1;
			if(pos[i]-lastpos>t)
			{
				lastneed=pos[i];
			}
		}
	}
	if(pos[n-1]-lastpos>t)
		ans++;
	return ans;
}


int main()
{
	//freopen("hw3p4.in","r",stdin);
	//freopen("hw3p4.out","w",stdout);

	int t;
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d%d",&n,&k);
		for(int i=0;i<n;i++)
			scanf("%d",&pos[i]);
		sort(pos,pos+n);
		int left=0;
		int right=pos[n-1]-pos[0];
		while(left!=right)
		{
			int mid=left+(right-left)/2;
			int nd=airportneeded(mid);
			if(nd>k)
				left=mid+1;
			else
				right=mid;
		}
		printf("%d\n",left);
	}
	return 0;
}