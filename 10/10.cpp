#include<iostream>
#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;

#define maxn 100005

int t,n;
int a[maxn];
int b[maxn];

int main()
{
	//freopen("hw5p5.in","r",stdin);
	scanf("%d",&t);
	while(t--){
		memset(b,-1,sizeof(b)); // b[i]==-1 means it has not been set
		scanf("%d",&n);
		// for convenience, we use label numbers 0 to n-1, rather than 1 to n
		int maxa=-1; // maxa: the highest IQ
		for(int i=0;i<n;i++){
			scanf("%d",&a[i]);
			maxa=max(maxa,a[i]);
		}
		for(int i=0;i<n;i++){
			if(a[i]==maxa){
				b[i]=-2; // the smartest SmalliKuos has b[i]=-2
			}
		}		
		for(int i=0;i<n;i++){
			if(b[i]==-2) // Nothing to do with the smartest guys
				continue;
			int ptr=(i-1+n)%n; // search from her left neighbor
			while(a[ptr]<=a[i]){ // loop until a smarter guy is found
				if(b[ptr]!=-1)
					ptr=b[ptr]; // the next guy who is possibly smarter than SmalliKuo
				else
					ptr=(ptr-1+n)%n;
			}
			b[i]=ptr;
		}
		bool first=true; // deal with space " "
		for(int i=0;i<n;i++){
			if(i==0)
				first=false;
			else
				printf(" ");
			if(b[i]==-2)
				printf("0");
			else
				printf("%d",b[i]+1);
		}
		printf("\n");
	}
	return 0;
}



